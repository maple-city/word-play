/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  staticPageGenerationTimeout: 120,
  output: "export",
  images: { unoptimized: true },
};

export default nextConfig;
