"use client"

import { Suspense, useEffect, useRef, useState } from "react";
import { DndProvider } from 'react-dnd';
import { TouchBackend } from "react-dnd-touch-backend";

import Canvas from "../ui/Canvas";
import Operations from "../ui/canvas-operations";
import Tools from "../ui/canvas-tools";
import { ImagePreview } from "../ui/ImageResult";
import Search from "../ui/AutoSearch";
import Paper from "../ui/Paper";
import { fitScreen, getDPI } from "../lib/utils";
import Image from "next/image";
import Settings from "../ui/ColoringPageSettings";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDownLeftAndUpRightToCenter, faUpRightAndDownLeftFromCenter } from "@fortawesome/free-solid-svg-icons";
import useLoadColoringPage from '../lib/hooks/useLoadColoringPage';


function Main() {
  const [canvas, setCanvas] = useState(null);
  const dpiRef = useRef(96);
  const [drawerSize, setDrawerSize] = useState(250);
  const [canvasWH, setCanvasWH] = useState({ width: 0, height: 0 });
  const [focusing, setFocusing] = useState(false);
  const [pageData, setPageData] = useLoadColoringPage();

  useEffect(() => {
    const updateCanvasWidthAndHeight = () => {
      const { width, height } = fitScreen(pageData.x, pageData.y, dpiRef.current);
      setCanvasWH({ width, height });
    };

    updateCanvasWidthAndHeight();
    window.addEventListener("resize", updateCanvasWidthAndHeight);

    return () => {
      window.removeEventListener("resize", updateCanvasWidthAndHeight);
    };
  }, [pageData]);

  const updateImageSizes = () => {
    setDrawerSize(Math.min(window.innerHeight / 3, 250));
  };

  useEffect(() => {
    updateImageSizes();
    window.addEventListener('resize', updateImageSizes);
    return () => window.removeEventListener('resize', updateImageSizes);
  }, []);

  useEffect(() => {
    dpiRef.current = getDPI();
  }, []);

  const focusMode = () => {
    setFocusing(!focusing);
  }

  return (
    <>
      <Paper width={canvasWH.width} height={canvasWH.height} />
      <DndProvider backend={TouchBackend} options={{ enableMouseEvents: true, ignoreContextMenu: true }}>
        <div className="top z-50">
          <div className="mt-1 flex items-center p-1 flex-nowrap">
            <a href="/" className="flex items-center">
              <Image priority={true} className="min-w-10 max-w-10" alt="light icon" src="/light-icon.svg" width="40" height="40" />
            </a>
            {!focusing &&
              <Settings settings={pageData} onSettingsChange={
                (newSettings: any) => {
                  setPageData(newSettings);
                }
              } />
            }
          </div>
          {!focusing &&
            <div className="mt-1 flex items-center flex-nowrap min-w-40 max-w-40">
              <Search drawerSize={drawerSize} />
            </div>
          }
        </div>
        <ImagePreview imageHeight={drawerSize} />
        <Canvas canvasObject={pageData.canvas} onCanvasReady={setCanvas} imageHeight={drawerSize} width={canvasWH.width} height={canvasWH.height} x={pageData.x} y={pageData.y} />
        <div className="left-center ml-2 z-50">
          <Tools canvas={canvas} hasFocus={focusing} />
          <Suspense>
            <Operations canvas={canvas} x={pageData.x} y={pageData.y} dpi={dpiRef.current} hasFocus={focusing} pageSettings={pageData} />
          </Suspense>
        </div>
        <div className="bottom-right p-2">
          <FontAwesomeIcon onTouchStart={focusMode} onClick={focusMode} size="sm" icon={focusing ? faDownLeftAndUpRightToCenter : faUpRightAndDownLeftFromCenter} />
        </div>
      </DndProvider>
    </>
  );
}

export default function App() {
  return (
    <Suspense>
      <Main />
    </Suspense>
  )
}
