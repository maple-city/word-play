import type { Metadata } from "next";

import '../setup';
import { Suspense } from "react";


export const metadata: Metadata = {
  title: "Make Coloring Pages",
  description: "Make coloring pages by mishmashing images into a beautiful work of art you can color to your heart's desire.",
  keywords: ["coloring pages", "coloring page", "colouring pages", "coloring page", "coloring page designer", "colouring page designer", "coloring page design", "colouring page design"],
  robots: { index: true, follow: false },
  alternates: { canonical: "https://makemecolorful.com/design" },
  icons: [
    {
      url: '/light-icon.svg',
      media: '(prefers-color-scheme: light)',
    },
    {
      url: '/dark-icon.svg',
      media: '(prefers-color-scheme: dark)',
    },
  ],
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" className="design">
      <body className="design">{children}</body>
    </html>
  );
}
