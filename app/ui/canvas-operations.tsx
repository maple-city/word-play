"use client"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPrint, faRotateLeft, faRotateRight, faSave } from "@fortawesome/free-solid-svg-icons"
import { useReactToPrint } from 'react-to-print';
import { useEffect, useState } from 'react';
import { cloneCanvas, scaleCanvasWindow } from "../lib/canvas";
import { useRouter, useSearchParams, usePathname } from "next/navigation";
import { save } from '../lib/save';

export default function Operations({ canvas, x, y, dpi, hasFocus, pageSettings }: any) {
  const [copiedObject, setCopiedObject] = useState<any>(null);
  let clonedCanvas: any = null;
  const router = useRouter();
  const searchParams = useSearchParams()
  const pathname = usePathname()

  function undo() {
    canvas.undo();
  }
  function redo() {
    canvas.redo();
  }
  const printHandler = useReactToPrint({
    content: () => {
      console.log("cloned canvas", clonedCanvas);
      return clonedCanvas.wrapperEl;
    },
    onBeforeGetContent: () => {
      clonedCanvas = cloneCanvas(canvas, canvas.getWidth(), canvas.getHeight());
      scaleCanvasWindow(clonedCanvas, x * dpi, y * dpi);
    },
    onBeforePrint: () => { },
    onAfterPrint: () => {
      if (clonedCanvas) {
        clonedCanvas.dispose();
        const canvasEl = clonedCanvas.wrapperEl;
        if (canvasEl && canvasEl.parentNode) {
          canvasEl.parentNode.removeChild(canvasEl);
        }
        clonedCanvas = null;
      }
    },
    removeAfterPrint: false,
    pageStyle: `
          @media print {
            @page {
              size: ${x}in ${y}in;
            }
          }`
  })


  useEffect(() => {

    const copy = () => {
      const activeObject = canvas.getActiveObject();

      if (activeObject) {
        activeObject.clone(function (cloned: any) {
          setCopiedObject(cloned);
        });
      }
    };
    const paste = () => {
      if (copiedObject) {
        copiedObject.clone(function (clonedObj: any) {
          clonedObj.set({
            left: copiedObject.left + 20,
            top: copiedObject.top + 20,
            evented: true
          });

          canvas.add(clonedObj);

          canvas.setActiveObject(clonedObj);

          canvas.renderAll();
        });
      }
    };
    const deleteObject = () => {
      const activeObject = canvas.getActiveObject();
      if (activeObject) {
        canvas.remove(activeObject);
        canvas.renderAll();
      }
    };
    const deselect = () => {
      canvas.discardActiveObject();
      canvas.renderAll();
    };

    const handleKeyDown = (e: any) => {
      if ((e.ctrlKey || e.metaKey) && e.key === 'c') {
        copy();
      } else if ((e.ctrlKey || e.metaKey) && e.key === 'v') {
        paste();
      } else if (e.key === 'Delete' || e.key === 'Backspace') {
        deleteObject();
      } else if (e.key === 'Escape') {
        deselect();
      }
    };

    document.addEventListener('keydown', handleKeyDown);

    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [canvas, copiedObject]);

  return (
    <>
      {!hasFocus &&
        <div className="mt-[10px] mb-[10px] p1 flex flex-col">
          <FontAwesomeIcon size="sm" className="icon" icon={faRotateLeft} onClick={undo} />
          <FontAwesomeIcon size="sm" className="icon" icon={faRotateRight} onClick={redo} />
          <FontAwesomeIcon size="sm" className="icon hover:bg-gray-100" icon={faPrint} onClick={printHandler} />
          <FontAwesomeIcon className="icon" icon={faSave} onClick={() => save({ canvas, searchParams, pathname, router, pageSettings })} />
        </div>
      }
    </>
  );
}

