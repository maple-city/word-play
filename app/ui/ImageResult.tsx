"use client"

import { useEffect, useRef, useState } from 'react';
import { useDrag } from 'react-dnd';
import { usePreview } from 'react-dnd-preview';


export const ImagePreview = ({ imageHeight }: any) => {
  const preview = usePreview()
  if (!preview.display) {
    return null;
  }
  const { itemType, item, style } = preview;
  const styles = {
    ...style,
    height: `${imageHeight}px`
  }
  return <img
    alt="Preview of Coloring Page"
    src={(item as { src: string }).src}
    style={styles}
    crossOrigin='anonymous'
  />
}


export default function DraggableImage({ src, height, sendEvent, hit }: any) {
  const [isHolding, setIsHolding] = useState(false);
  const holdTimeout = useRef(null) as any;
  const canDragRef = useRef(false);

  const [{ isDragging }, drag, dragPreview] = useDrag(
    () => ({
      type: 'image',
      item: (monitor) => {
        return {
          "src": src,
          ...monitor.getClientOffset(),
          "hit": hit,
          "sendEvent": sendEvent
        }
      },
      canDrag: () => canDragRef.current,
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
      end: (item, monitor) => {
        if (monitor.didDrop()) {
          canDragRef.current = false;
          setIsHolding(false);
        }
      },
    }),
    []
  ) as any;

  const handleMouseDown = (e: any) => {
    holdTimeout.current = setTimeout(() => {
      setIsHolding(true);
      canDragRef.current = true;
    }, 0);
  };

  const handleMouseUp = () => {
    clearTimeout(holdTimeout.current);
    setIsHolding(false);
  };

  const handleTouchStart = (e: any) => {
    holdTimeout.current = setTimeout(() => {
      setIsHolding(true);
      canDragRef.current = true;
    }, 300);
  };

  const handleTouchEnd = (e: any) => {
    clearTimeout(holdTimeout.current);
    setIsHolding(false);
  };

  const handleContextMenu = (e: any) => {
    e.preventDefault();
  };

  useEffect(() => {
    const handleWindowMouseUp = () => {
      handleMouseUp();
    };
    const handleWindowTouchEnd = (e: any) => {
      handleTouchEnd(e);
    }

    window.addEventListener('mouseup', handleWindowMouseUp);
    window.addEventListener('touchend', handleWindowTouchEnd);

    return () => {
      window.removeEventListener('mouseup', handleWindowMouseUp);
      window.removeEventListener('touchend', handleWindowTouchEnd);
    };
  }, []);

  return (
    <img
      alt="Coloring Page"
      crossOrigin="anonymous"
      ref={drag}
      src={src}
      draggable={false}
      style={{
        opacity: isDragging ? 0.5 : 1,
        height: `${height}px`,
        border: isHolding ? '2px solid blue' : 'none',
        cursor: isHolding ? 'move' : 'default',
        touchAction: 'manipulation',
      }}
      onMouseDown={handleMouseDown}
      onMouseUp={handleMouseUp}
      onTouchStart={handleTouchStart}
      onTouchEnd={handleTouchEnd}
      onContextMenu={handleContextMenu}
    />
  );
}
