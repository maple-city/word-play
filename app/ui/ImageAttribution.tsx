"use client"

import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  CardBody,
  CardFooter,
  Typography,
  Button,
  Popover,
  PopoverHandler,
  PopoverContent
} from "@material-tailwind/react";

export default function Attribution({ author, credit, derivation, permission, referer }: any) {
  return (
    <div className="relative">
      <Popover placement="top" >
        <PopoverHandler>
          <FontAwesomeIcon size="sm" className="top-0 p-1 left-1/2 absolute text-purple-600 hover:text-blue-600" icon={faInfoCircle} />
        </PopoverHandler>
        <PopoverContent className="z-50" placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>
          <CardBody className="p-2 w-96" placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>
            {author && <Typography variant="h5" color="blue-gray" className="mb-2" placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>
              {author}
            </Typography>}
            {credit && <Typography variant="h4" color="blue-gray" className="mb-2" placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>
              {credit}
            </Typography>}
            {derivation && <Typography variant="h3" color="blue-gray" className="mb-2" placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>
              {derivation}
            </Typography>}
            <Typography placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>{permission}</Typography>
          </CardBody>
          <CardFooter className="p-2" placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>
            <a rel="noreferrer" href={referer} target="_blank">
              <Button placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>Visit Source</Button>
            </a>
          </CardFooter>
        </PopoverContent>
      </Popover>
    </div>
  );
}