import { useEffect, useState } from "react";


export default function Paper({ width, height }: any) {
  const [dimensions, setDimensions] = useState({ width: 0, height: 0 });
  const [positions, setPositions] = useState({ top: 0, bottom: 0, left: 0, right: 0 });
  const [windowSize, setWindowSize] = useState({ width: 0, height: 0 });

  const handleResize = () => {
    setDimensions({ width, height });
    const vh = window.innerHeight;
    const vw = window.innerWidth;
    const top = 0;
    const bottom = vh;
    const left = 0;
    const right = vw;
    setPositions({ top, bottom, left, right });
  };

  useEffect(() => { handleResize(); }, [width, height]);

  useEffect(() => {
    const updateWindowSize = () => {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    };
    window.addEventListener('resize', updateWindowSize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => { handleResize(); }, [windowSize]);

  return (
    <>
      <div className="paper-outside" style={{
        position: "fixed",
        top: positions.top,
        height: `${positions.top + (positions.bottom - dimensions.height) / 2}px`,
        width: `${positions.right}px`,
      }} />
      <div className="paper-outside" style={{
        position: "fixed",
        top: `${positions.bottom - (positions.bottom - dimensions.height) / 2}px`,
        height: `${positions.top + (positions.bottom - dimensions.height) / 2}px`,
        width: `${positions.right}px`,
      }} />
      <div className="paper-outside" style={{
        position: "fixed",
        left: positions.left,
        top: `${positions.top + (positions.bottom - dimensions.height) / 2}px`,
        height: `${dimensions.height}px`,
        width: `${(positions.right - dimensions.width) / 2}px`,
      }} />
      <div className="paper-outside" style={{
        position: "fixed",
        left: `${positions.right - (positions.right - dimensions.width) / 2}px`,
        top: `${positions.top + (positions.bottom - dimensions.height) / 2}px`,
        height: `${dimensions.height}px`,
        width: `${(positions.right - dimensions.width) / 2}px`,
      }} />
      <div className="flex fixed justify-center items-center w-screen h-screen">
        <div className="border border-black/10  rounded-md"
          style={{
            width: `${dimensions.width}px`,
            height: `${dimensions.height}px`,
            boxShadow: '0 4px 6px rgba(0, 0, 0, 0.1), 0 -4px 6px rgba(0, 0, 0, 0.1), 4px 0 6px rgba(0, 0, 0, 0.1), -4px 0 6px rgba(0, 0, 0, 0.1)'
          }} />
      </div>
    </>
  );
}