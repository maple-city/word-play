"use client"

import { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowPointer, faEraser } from "@fortawesome/free-solid-svg-icons"
import { fabric } from "fabric-with-erasing";

export default function Tools({ canvas, hasFocus }: any) {
    const [mode, setMode] = useState('hand');

    function handMode() {
        if (canvas) {
            canvas.isDrawingMode = false;
            canvas.defaultCursor = "auto";
            fabric.Object.prototype.selectable = true;
            setMode('hand');
        }
    }
    function eraserMode() {
        if (canvas) {
            canvas.isDrawingMode = true;
            canvas.defaultCursor = "crosshair";
            canvas.freeDrawingBrush = new fabric.EraserBrush(canvas);
            canvas.freeDrawingBrush.width = 20;
            fabric.Object.prototype.selectable = false;
            setMode('eraser');
        }
    }
    useEffect(() => { handMode() }, [canvas]);

    return (
        <>
            {!hasFocus && (
                <div className="mt-[10px] mb-[10px] p-1 flex flex-col">
                    <FontAwesomeIcon
                        size="sm"
                        className="icon"
                        icon={faArrowPointer}
                        onClick={handMode}
                        style={{ color: mode === 'hand' ? 'orange' : 'black' }}
                    />
                    <FontAwesomeIcon
                        size="sm"
                        className="icon"
                        icon={faEraser}
                        onClick={eraserMode}
                        style={{ color: mode === 'eraser' ? 'palevioletred' : 'black' }}
                    />
                </div>
            )}
        </>
    );
}
