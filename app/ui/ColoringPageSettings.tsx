"use client"

import {
  Dialog,
  DialogBody,
  Input,
  Select,
  Option,
  DialogFooter,
  Button,
} from "@material-tailwind/react";
import { useEffect, useRef, useState } from "react";

export default function Settings({ settings, onSettingsChange }: any) {
  const [open, setOpen] = useState(false);
  const [pageOrientation, setPageOrientation] = useState(settings.orientation);
  const [pageTitle, setPageTitle] = useState(settings.title);
  const handleOpen = () => setOpen(!open);
  const inputRef = useRef(null) as any;

  useEffect(() => {
    if (open && inputRef.current) {
      const timer = setTimeout(() => {
        inputRef.current.blur();
      }, 0);
      return () => clearTimeout(timer);
    }
  }, [open]);

  useEffect(() => {
    setPageTitle(settings.title)
    setPageOrientation(settings.orientation)
  }, [settings]);

  const onTitleChange = ({ target }: any) => setPageTitle(target.value);
  const onOrientationChange = (orientation: any) => {
    setPageOrientation(orientation);
  };


  const onPageSettingsChange = () => {
    const newSettings = {
      ...settings,
      x: pageOrientation == "landscape" ? 11 : 8.5,
      y: pageOrientation == "landscape" ? 8.5 : 11,
      orientation: pageOrientation,
      title: pageTitle
    }
    console.log("new settings", newSettings);
    onSettingsChange(newSettings);
  }

  return (
    <>
      <div className="p-2 hov" onClick={handleOpen}>
        <p className="max-w-40 min-w-40 text-gray-500 text-nowrap text-sm text-ellipsis overflow-hidden">{pageTitle}</p>
      </div>
      <Dialog size="xs" open={open} handler={handleOpen} placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>
        <DialogBody placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>
          <div className="flex flex-col gap-6">
            <Input inputRef={inputRef} label="Coloring Page Name" onChange={onTitleChange} value={pageTitle} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined} crossOrigin={undefined} />
            <Select
              label="Page Orientation"
              onChange={onOrientationChange}
              value={pageOrientation}
              placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>
              <Option value="portrait">Portrait</Option>
              <Option value="landscape">Landscape</Option>
            </Select>
          </div>
          <DialogFooter placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>
            <Button placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}
              variant="text"
              color="red"
              onClick={handleOpen}
              className="mr-1"
            >
              <span>Cancel</span>
            </Button>
            <Button
              placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}
              variant="gradient"
              color="purple"
              onClick={() => {
                onPageSettingsChange();
                handleOpen();
              }}>
              <span>Confirm</span>
            </Button>
          </DialogFooter>

        </DialogBody>
      </Dialog>
    </>
  );
}