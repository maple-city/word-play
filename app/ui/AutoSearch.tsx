"use client"

import algoliasearch, { SearchClient } from 'algoliasearch/lite';
import { autocomplete, AutocompleteOptions } from '@algolia/autocomplete-js';
import { createLocalStorageRecentSearchesPlugin } from "@algolia/autocomplete-plugin-recent-searches";
import { BaseItem } from "@algolia/autocomplete-core";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpLong } from "@fortawesome/free-solid-svg-icons"

import { Drawer } from "@material-tailwind/react";

import { createElement, Fragment, useRef, useEffect, useMemo, useState } from 'react';
import { createRoot } from 'react-dom/client';
import { InstantSearch, PoweredBy, useInstantSearch, useInfiniteHits, useSearchBox, usePagination } from 'react-instantsearch';

import DraggableImage from "./ImageResult"
import Attribution from './ImageAttribution';


const algoliaClient = algoliasearch("POIFCL6OW2", "3ef7c980ac236eb19ec0faf36eabe9be");

const searchClient: SearchClient = {
  ...algoliaClient,
  search(requests) {
    if (requests.every(({ params }) => params && !params.query)) {
      return Promise.resolve({
        results: requests.map(() => ({
          hits: [],
          nbHits: 0,
          nbPages: 0,
          page: 0,
          processingTimeMS: 0,
          hitsPerPage: 0,
          exhaustiveNbHits: false,
          query: '',
          params: '',
        })),
      });
    }

    return algoliaClient.search(requests as any);
  },
};


function Hit({ hit, imageHeight, sendEvent }: any) {
  const coloringPagesDomain = "https://images.makemecolorful.com/";
  let author = hit.extra_fields["Author"];
  const index = author ? author.indexOf("This coloring page is") : -1;
  let derivation = "";
  if (index > -1) {
    author = author.substring(0, index).trim();
    derivation = author.substring(index, author.length).trim();
  }
  let text = "";
  if (author) {
    text += `<div class="author">${author}</div><hr>`;
  }
  if (derivation) {
    text += `<div class="derivation">${derivation}</div><hr>`;
  }
  const src = coloringPagesDomain + hit.objectID;

  return (
    <article>
      <Attribution referer={hit.referer} author={author} credit={hit.extra_fields["Original image credit"]} derivation={derivation} permission={hit.extra_fields["Permission"]} />
      <DraggableImage src={src} height={imageHeight} sendEvent={sendEvent} hit={hit} />
    </article>
  )
}

function InfiniteHits(props: any) {
  const { hits, isLastPage, showMore, sendEvent } = useInfiniteHits(props);
  const sentinelRef = useRef(null);

  useEffect(() => {
    if (sentinelRef.current !== null) {
      const observer = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting && !isLastPage) {
            showMore();
          }
        });
      });

      observer.observe(sentinelRef.current);

      return () => {
        observer.disconnect();
      };
    }
  }, [isLastPage, showMore]);

  return (
    <div className="ais-InfiniteHits">
      <ul className="ais-InfiniteHits-list">
        {hits.map((hit) => (
          <li key={hit.objectID} className="ais-InfiniteHits-item">
            <Hit hit={hit} imageHeight={props.imageHeight} sendEvent={sendEvent} />
          </li>
        ))}
        <li ref={sentinelRef} aria-hidden="true">&nbsp;</li>
      </ul>
    </div>
  );
}

function NoResultsBoundary({ children, fallback }: any) {
  const { results } = useInstantSearch();

  // The `__isArtificial` flag makes sure not to display the No Results message
  // when no hits have been returned.
  if (!results.__isArtificial && results.nbHits === 0 && results.query != "") {
    return (
      <>
        {fallback}
        <div hidden>{children}</div>
      </>
    );
  }

  return children;
}

function NoResults({ imageHeight }: any) {
  const { indexUiState } = useInstantSearch();

  return (
    <div className="flex items-center absolute left-1/2 -translate-x-1/2">
      <div className="flex items-end">
        <img alt="placeholder" draggable="false" style={{ height: `${imageHeight}px` }} className="filter-purple" src="/2018-07-sleeping-fox-coloring-page.png" />
        <p className="text-sm">Sorry! Nothing for <q>{indexUiState.query}</q></p>
      </div>
    </div>
  );
}

function EmptyQueryBoundary({ children, fallback, imageHeight }: any) {
  const { indexUiState } = useInstantSearch();

  if (!indexUiState.query) {
    return (
      <>
        {fallback}
        <div hidden>{children}</div>
        <div className="flex items-center absolute left-1/2 -translate-x-1/2">
          <div className="flex items-end">
            <img alt="placeholder" draggable="false" style={{ height: `${imageHeight}px` }} className="filter-purple" src="/2022-01-cartoon-traveller-lady-looking-at-the-map-coloring-page.png" />
            <p className="text-sm">Start your search...</p>
          </div>
        </div>
      </>
    );
  }

  return children;
}

const IconToggle = ({ open, openDrawer }: any) => {
  const { query } = useSearchBox();
  return (
    <div className="bottom-center" style={{ visibility: query && !open ? 'visible' : 'hidden' }}>
      <FontAwesomeIcon size="sm" onClick={openDrawer} onMouseEnter={openDrawer} className="text-2xl p-4 text-gray-500" icon={faUpLong} />
    </div>
  );
};


type AutocompleteProps = Partial<AutocompleteOptions<BaseItem>> & {
  className?: string;
  openDrawer: () => void;
};

type SetInstantSearchUiStateOptions = {
  query: string;
};

export function Autocomplete({
  className,
  openDrawer,
  ...autocompleteProps
}: AutocompleteProps) {
  const autocompleteContainer = useRef<HTMLDivElement>(null);
  const panelRootRef = useRef<any>(null);
  const rootRef = useRef<any>(null);

  const { query, refine: setQuery } = useSearchBox();
  const { refine: setPage } = usePagination();

  const [instantSearchUiState, setInstantSearchUiState] = useState<
    SetInstantSearchUiStateOptions
  >({ query });

  useEffect(() => {
    const handleFocus = () => {
      openDrawer();
    };

    const rootElement = autocompleteContainer.current;

    if (rootElement) {
      rootElement.addEventListener('focus', handleFocus, true);
    }

    return () => {
      if (rootElement) {
        rootElement.removeEventListener('focus', handleFocus, true);
      }
    };
  }, [openDrawer]);

  useEffect(() => {
    setQuery(instantSearchUiState.query);
    setPage(0);
  }, [instantSearchUiState]);


  const plugins = useMemo(() => {
    const recentSearches = createLocalStorageRecentSearchesPlugin({
      key: "instantsearch",
      limit: 5,
      transformSource({ source }) {
        return {
          ...source,
          onSelect({ item }) {
            setInstantSearchUiState({ query: item.label });
          }
        };
      }
    });

    return [recentSearches];
  }, []);

  useEffect(() => {
    if (!autocompleteContainer.current) {
      return;
    }

    const autocompleteInstance = autocomplete({
      ...autocompleteProps,
      container: autocompleteContainer.current,
      initialState: { query },
      onReset() {
        setInstantSearchUiState({ query: "" });
      },
      onSubmit({ state }) {
        setInstantSearchUiState({ query: state.query });
      },
      onStateChange({ prevState, state }) {
        if (prevState.query !== state.query) {
          setInstantSearchUiState({
            query: state.query
          });
        }
      },
      renderer: { createElement, Fragment, render: () => { } },
      render({ children }, root) {
        if (!panelRootRef.current || rootRef.current !== root) {
          rootRef.current = root;

          panelRootRef.current?.unmount();
          panelRootRef.current = createRoot(root);
        }

        panelRootRef.current.render(children);
      },
      plugins,
    });
    return () => autocompleteInstance.destroy();
  }, [plugins]);

  return <div className={className} ref={autocompleteContainer} />;
}

export default function Search({ drawerSize }: any) {
  const [open, setOpen] = useState(false);

  const openDrawer = () => setOpen(true);
  const closeDrawer = () => {
    const handleDocumentClick = (event: TouchEvent | MouseEvent) => {
      if (event.target instanceof HTMLCanvasElement) {
        setOpen(false);
        document.removeEventListener('touchstart', handleDocumentClick);
        document.removeEventListener('click', handleDocumentClick);
      }
    };
    document.addEventListener('touchstart', handleDocumentClick);
    document.addEventListener('click', handleDocumentClick);
  };



  return (
    <InstantSearch searchClient={searchClient} indexName="coloring-pages" insights={true}>
      <Autocomplete
        placeholder="Search image library..."
        detachedMediaQuery="none"
        openOnFocus={true}
        openDrawer={openDrawer}
      />
      <IconToggle open={open} openDrawer={openDrawer} />
      <Drawer size={drawerSize} placement="bottom" open={open} onClose={closeDrawer} className="opacity-80" overlay={false} placeholder={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined}>
        <EmptyQueryBoundary imageHeight={drawerSize} fallback={null}>
          <NoResultsBoundary fallback={<NoResults imageHeight={drawerSize} />}>
            <InfiniteHits imageHeight={drawerSize} />
            <PoweredBy />
          </NoResultsBoundary>
        </EmptyQueryBoundary>
      </Drawer>
    </InstantSearch>
  )
}