"use client"

import { fabric } from "fabric-with-erasing";
import { useEffect, useRef, useState } from 'react';
import { useDrop } from 'react-dnd'
import { cloneCanvas, resizeCanvas } from "../lib/canvas";
import { join } from "path";


export default function Canvas({ canvasObject, onCanvasReady, imageHeight, width, height, x, y }: any) {
    const canvasRef = useRef(null);
    const [canvasInstance, setCanvasInstance] = useState<any>(null);
    const canvasInitialized = useRef(false) as any;
    const jsonLoaded = useRef(false) as any;

    const [, drop] = useDrop(() => ({
        accept: "image",
        drop: (item: any, monitor: any) => {
            console.log("dropping", item, monitor)
            const offsetInit = monitor.getInitialSourceClientOffset();

            const xWithinImage = item.x - offsetInit.x;
            const yWithinImage = item.y - offsetInit.y;

            const canvasElement = canvasRef.current as any;
            const canvasRect = canvasElement.getBoundingClientRect();

            const dropOffset = monitor.getClientOffset();
            const canvasX = dropOffset.x - canvasRect.left - xWithinImage;
            const canvasY = dropOffset.y - canvasRect.top - yWithinImage;

            fabric.Image.fromURL(item.src, (image: any) => {
                image.set({
                    left: canvasX,
                    top: canvasY
                });
                image.scaleToHeight(imageHeight);
                canvasInstance.add(image);
            }, { crossOrigin: 'anonymous' }
            );

            item.sendEvent("click", item.hit, "Image Clicked");

        }
    }), [canvasInstance, imageHeight]);

    useEffect(() => {
        async function initializeCanvas() {
            await import('fabric-history');

            const canvas = new fabric.Canvas(canvasRef.current);
            fabric.Object.prototype.selectable = false;

            if (typeof canvas.undo === 'undefined' || typeof canvas.redo === 'undefined') {
                console.error("Undo and redo methods are not available. Ensure fabric-history is properly imported.");
            } else {
                console.log("Undo and redo methods are available.");
            }
            setCanvasInstance(canvas)

            if (typeof onCanvasReady === 'function') {
                onCanvasReady(canvas);
            }
        }
        if (!canvasInitialized.current) {
            initializeCanvas();
            canvasInitialized.current = true;
        }

        return () => {
            if (canvasInstance) {
                canvasInstance.dispose();
            }
        };
    }, [onCanvasReady]);

    useEffect(() => {
        if (
            canvasInstance && Object.keys(canvasInstance).length > 0
            && canvasObject && Object.keys(canvasObject).length > 0
            && x / y == width / height
            && !jsonLoaded.current
        ) {
            canvasInstance.loadFromJSON(canvasObject, () => {
                console.log(canvasObject);
                console.log("JSON loaded successfully", canvasInstance.width, canvasInstance.height, width, height);
                jsonLoaded.current = true;
            });
        }
    }, [canvasInstance, canvasObject, width, height])

    useEffect(() => {
        if ((canvasInstance && Object.keys(canvasInstance).length > 0) && (width != canvasInstance.width || height != canvasInstance.height)) {
            resizeCanvas(canvasInstance, width, height);
        }
    }, [width, height]);

    return (
        <div
            ref={drop as any} className="h-screen w-screen flex items-center justify-center relative">
            <canvas
                ref={canvasRef}
                width={width}
                height={height}>
            </canvas>
        </div>
    );
}
