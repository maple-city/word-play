import type { Metadata } from "next";

import './setup';


export const metadata: Metadata = {
    title: "Make Me Colorful",
    description: "A web site dedicated to help people create coloring pages from a library of 40k+ images under Public Domain or Commercial Creative-Commons Licenses.",
    keywords: ["coloring pages", "coloring page", "colouring pages", "coloring page", "coloring page designer", "colouring page designer", "coloring page design", "colouring page design"],
    robots: { index: true, follow: false },
    alternates: { canonical: "https://makemecolorful.com" },
    icons: [
        {
            url: '/light-icon.svg',
            media: '(prefers-color-scheme: light)',
        },
        {
            url: '/dark-icon.svg',
            media: '(prefers-color-scheme: dark)',
        },
    ],
};

export default function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang="en">
            <body>{children}</body>
        </html>
    );
}
