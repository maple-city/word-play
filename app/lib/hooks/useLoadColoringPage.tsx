import { useEffect, useState } from 'react';
import { useSearchParams } from 'next/navigation';

interface PageData {
  title: string;
  orientation: string;
  x: number;
  y: number;
  canvas: object;
}

function useLoadColoringPage(): [PageData, React.Dispatch<React.SetStateAction<PageData>>] {
  const searchParams = useSearchParams();

  const defaultPage = {
    title: "Untitled Coloring Page",
    orientation: "portrait",
    x: 8.5,
    y: 11,
    canvas: {},
  }
  const [pageData, setPageData] = useState<PageData>(defaultPage);

  useEffect(() => {
    const id = searchParams.get("id");
    let loadedPage = defaultPage;

    if (id) {
      const localStorageKey = 'MMC_designs';
      const storedPagesString = localStorage.getItem(localStorageKey);
      if (storedPagesString) {
        const storedPages = JSON.parse(storedPagesString);
        loadedPage = storedPages[id] || defaultPage;
      }
    }
    setPageData(loadedPage);
  }, [searchParams]);

  return [pageData, setPageData];
}

export default useLoadColoringPage;
