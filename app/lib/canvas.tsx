import { fabric } from "fabric-with-erasing";

export function cloneCanvas(canvas: any, width: number, height: number) {
    const newCanvasEl = document.createElement('canvas');
    newCanvasEl.width = width;
    newCanvasEl.height = height;
    const newCanvas = new fabric.Canvas(newCanvasEl);

    const originalObjects = canvas.getObjects();
    originalObjects.forEach((obj: any) => {
        const clone = fabric.util.object.clone(obj);
        clone.left -= (canvas.getWidth() - width) / 2;
        clone.top -= (canvas.getHeight() - height) / 2;
        newCanvas.add(clone);
    });

    newCanvas.renderAll();
    return newCanvas;
}

export function resizeCanvas(canvas: any, width: number, height: number) {
    if (!canvas) return;

    const originalWidth = canvas.getWidth();
    const originalHeight = canvas.getHeight();
    const scaleFactorX = width / originalWidth;
    const scaleFactorY = height / originalHeight;

    canvas.getObjects().forEach((obj: any) => {
        // this doesn't work well when changing page orientation
        // obj.scaleX *= scaleFactorX;
        // obj.scaleY *= scaleFactorY;
        obj.left *= scaleFactorX;
        obj.top *= scaleFactorY;
        obj.setCoords();
    });

    canvas.setWidth(width);
    canvas.setHeight(height);
}

export function scaleCanvasWindow(canvas: any, width: number, height: number) {
    if (!canvas) return;

    const originalWidth = canvas.getWidth();
    const originalHeight = canvas.getHeight();
    const scaleFactorX = width / originalWidth;
    const scaleFactorY = height / originalHeight;

    canvas.getObjects().forEach((obj: any) => {
        obj.scaleX *= scaleFactorX;
        obj.scaleY *= scaleFactorY;
        obj.left *= scaleFactorX;
        obj.top *= scaleFactorY;
        obj.setCoords();
    });

    canvas.setWidth(width);
    canvas.setHeight(height);
}