import { v4 as uuidv4 } from 'uuid';

export function save({ canvas, searchParams, pathname, router, pageSettings }: any) {
    let coloringPageId = searchParams.get('id');
    if (coloringPageId === undefined || coloringPageId === null) {
        coloringPageId = uuidv4();
        const url = `${pathname}?${searchParams}&id=${coloringPageId}`;
        router.push(url);
    }

    const data = canvas.toObject();

    pageSettings.id = coloringPageId;
    const coloringPage = {
        ...pageSettings,
        id: coloringPageId,
        lastUpdate: new Date().toISOString(),
        canvas: data,
    };

    const localStorageKey = 'MMC_designs';
    let storedPages: { [key: string]: any } = {};

    const storedPagesString = localStorage.getItem(localStorageKey);
    if (storedPagesString) {
        storedPages = JSON.parse(storedPagesString);
    }

    storedPages[coloringPageId] = coloringPage;
    localStorage.setItem(localStorageKey, JSON.stringify(storedPages));
}
