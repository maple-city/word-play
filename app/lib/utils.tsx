export function getDPI() {
    let div = document.createElement("div");
    div.style.width = "1in";
    div.style.height = "1in";
    div.style.position = "absolute";
    div.style.top = "-100%";
    document.body.appendChild(div);
    let dpi = div.offsetWidth;
    document.body.removeChild(div);
    return dpi;
}

export function fitScreen(x: number, y: number, dpi: number) {
    const aspectRatio = x / y;
    let width = x * dpi;
    let height = y * dpi;

    if (width > window.innerWidth) {
        width = window.innerWidth;
        height = width / aspectRatio;
    }
    if (height > window.innerHeight) {
        height = window.innerHeight;
        width = height * aspectRatio;
    }
    return { width, height }
}
