"use client"

import { faPlus, faMinus, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image';
import React, { useEffect, useRef, useState } from 'react';
import { fabric } from "fabric-with-erasing";
import ClipLoader from "react-spinners/ClipLoader";
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
dayjs.extend(relativeTime);


import { Red_Hat_Display } from "next/font/google";
import { getDPI } from './lib/utils';
const font = Red_Hat_Display({
    weight: "400",
    style: "normal",
    subsets: ["latin"]
});

const blurDataURL = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAH0lEQVQYV2P8z/CfAQ4AI3AAARUBxF9iBqJRDkHgD4RAAJwXxTzF2+jAAAAAElFTkSuQmCC";

function PagePreview({ page, dpi, toggleSelector, updateSelectedItemsArray }: any) {
    const [preview, setPreview] = useState({}) as any;
    const [previewHeight, setPreviewHeight] = useState(250) as any;
    const [selected, setSelected] = useState(false);

    useEffect(() => {
        if (toggleSelector) {
            setSelected(false);
        }
    }, [toggleSelector])

    useEffect(() => {
        const canvas = new fabric.Canvas();
        canvas.width = page.x * dpi;
        canvas.height = page.y * dpi;
        canvas.loadFromJSON(page.canvas, () => {
            const dataUrl = canvas.toDataURL();
            setPreview({ "dataUrl": dataUrl, "id": page.id });
        });

        return () => {
            if (canvas) {
                canvas.dispose();
            }
        };
    }, [page, dpi]);

    const handleSelected = () => {
        let flag = "remove";
        if (!selected) {  // this is opposite what you think because the state doesn't change yet
            flag = "add";
        }
        updateSelectedItemsArray(page.id, flag);
        setSelected(!selected);
    }

    return (
        <>
            <div className="outline outline-purple-100 max-w-sm rounded overflow-hidden shadow-lg relative">
                {
                    toggleSelector &&
                    <label className="absolute top-0 right-0 m-2 m-2 accent-purple-500/25">
                        <input type="checkbox" checked={selected} onChange={handleSelected} />
                    </label>
                }
                <a className="flex justify-center" href={`/design?id=${page.id}`}>
                    {preview.dataUrl ? (
                        <Image
                            src={preview.dataUrl}
                            alt="canvas-preview"
                            width={page.x * dpi}
                            height={page.y * dpi}
                            placeholder="blur"
                            blurDataURL={blurDataURL}
                            className="w-full border-b border-purple-100"
                            style={{ height: previewHeight, width: "auto" }}
                            draggable={false}
                        />
                    ) : (
                        <div
                            style={{ height: previewHeight, width: previewHeight * page.x / page.y }}
                            className="flex justify-center items-center"
                        >
                            <ClipLoader color={"#123abc"} loading={true} size={50} />
                        </div>
                    )}
                </a>
                <div className="px-2 py-2">
                    <a href={`/design?id=${page.id}`}>
                        <div className={`${font.className} font-bold text-xl mb-1 text-purple-500 hover:text-blue-600`}>{page.title}</div>
                    </a>
                </div>
                <div className="px-2 pt-2 pb-2">
                    <p className="text-xs text-gray-700"> Last update: <span>{dayjs(page.lastUpdate).fromNow()}</span></p>
                </div>

            </div>

        </>
    );
}


export default function App() {
    const [pages, setPages] = useState([]) as any;
    const dpiRef = useRef(96);
    const [minusPressed, setMinusPressed] = useState(false) as any;
    const [selectedPagesArray, setSelectedPagesArray] = useState([]) as any;
    const [loadingPages, setLoadingPages] = useState(true) as any;

    useEffect(() => {
        dpiRef.current = getDPI();
    }, []);

    useEffect(() => {
        setSelectedPagesArray([]);
    }, [minusPressed]);

    const handleLoadPagesFromLocalStorage = () => {
        const localStorageKey = 'MMC_designs';
        const storedPagesString = localStorage.getItem(localStorageKey);
        if (storedPagesString) {
            const storedPages = JSON.parse(storedPagesString);
            setPages(Object.values(storedPages));
        }
        setLoadingPages(false);
    };

    useEffect(() => {
        handleLoadPagesFromLocalStorage();
        const handleStorageChange = (event: any) => {
            if (event.key === 'MMC_designs') {
                handleLoadPagesFromLocalStorage();
            }
        };
        window.addEventListener('storage', handleStorageChange);
        return () => {
            window.removeEventListener('storage', handleStorageChange);
        };
    }, []);

    const onMinus = () => {
        setMinusPressed(!minusPressed);
    }

    const updateSelectedPagesArray = (selected: any, flag: string) => {
        const allSelected = [...selectedPagesArray] as any;
        if (flag == "add") {
            if (!allSelected.includes(selected)) {
                setSelectedPagesArray([...allSelected, selected]);
            }
        } else if (flag == "remove") {
            const updatedSelectedItems = allSelected.filter((item: any) => item !== selected);
            setSelectedPagesArray(updatedSelectedItems);
        }
    };

    const onDelete = () => {
        const localStorageKey = 'MMC_designs';
        const storedPagesString = localStorage.getItem(localStorageKey);
        if (storedPagesString) {
            const storedPages = JSON.parse(storedPagesString);

            for (const selected of selectedPagesArray) {
                delete storedPages[selected];
            }
            localStorage.setItem(localStorageKey, JSON.stringify(storedPages));
            handleLoadPagesFromLocalStorage();
        }
    }

    const handleTouch = () => {
        onMinus();
    };

    return (
        <>
            <div className="w-[100vw] h-[50px] top-0 left-0 flex items-center flex-nowrap">
                <div className="mt-1 flex items-center p-1 flex-nowrap">
                    <a href="/" className="flex items-center">
                        <Image priority={true} className="min-w-10 max-w-10 ml-2" alt="light icon" src="/light-icon.svg" width="40" height="40" />
                        <p className={`${font.className} whitespace-nowrap text-purple-600 hover:text-blue-600 text-md px-1 flex flex-nowrap items-center`}>
                            make me colorful
                        </p>
                    </a>
                </div>
            </div>
            <div className="flex items-center">
                <h1 className={`${font.className} m-4`}>
                    Your Pages
                </h1>
                <a href="/design">
                    <FontAwesomeIcon className="hov text-purple-400 hover:text-blue-600 m-2" size="lg" icon={faPlus} />
                </a>
                <FontAwesomeIcon
                    className="hov text-purple-400 hover:text-blue-600 m-2"
                    size="lg"
                    icon={faMinus}
                    onTouchStart={handleTouch}
                    onTouchEnd={handleTouch}
                    onClick={onMinus}
                />
                {
                    minusPressed && selectedPagesArray.length > 0 && <FontAwesomeIcon
                        className="hov text-purple-400 hover:text-blue-600 m-2"
                        icon={faTrash} onTouchStart={() => {
                            onDelete();
                            onMinus();
                        }} onClick={() => {
                            onDelete();
                            onMinus();
                        }}
                    />
                }

            </div>

            {!loadingPages && pages.length == 0 ? (
                <div className="flex justify-center">
                    <a href="/design">
                        <div className="hov outline-dashed outline-purple-100 max-w-3xl rounded cursor-pointer">
                            <div className="px-6 py-4">
                                <div className="px-6 py-4">
                                    <p className={`${font.className} text-xl`}> Let&apos;s make some coloring pages!</p>
                                </div>
                                <div className="flex justify-center items-center px-6 py-4">
                                    <FontAwesomeIcon className={"text-purple-400"} size="2xl" icon={faPlus} /> <span className={`${font.className} m-2 text-sm`}>Add a Coloring Page</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            ) : (
                <div className="flex overflow-x-scroll">
                    {pages
                        .sort((a: any, b: any) => {
                            const dateA = new Date(a.lastUpdate);
                            const dateB = new Date(b.lastUpdate);
                            return dateB.getTime() - dateA.getTime();
                        })
                        .map((page: any) => (
                            <div key={page.id} className="m-2 shrink-0 max-w-80">
                                <PagePreview page={page} dpi={dpiRef.current} toggleSelector={minusPressed} updateSelectedItemsArray={updateSelectedPagesArray} />
                            </div>
                        ))}
                </div>
            )}
        </>
    )
}
