import type { Config } from "tailwindcss";
import withMT from "@material-tailwind/react/utils/withMT";
import { RequiredConfig } from "tailwindcss/types/config";

const config: Config = withMT({
  content: ["./app/**/*.{js,ts,jsx,tsx,mdx}"],
  theme: {
    extend: {},
  },
  plugins: [],
}) as RequiredConfig;

export default config;
